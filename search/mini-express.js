const express = require('express')
const bodyParser = require('body-parser');
const cron = require('node-cron');
const fetch = require('node-fetch');
const puppeteer =  require('puppeteer');
const { PuppeteerBlocker } = require('@cliqz/adblocker-puppeteer');
// const puppeteer = require('puppeteer-extra')

// const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
// puppeteer.use(AdblockerPlugin())

function findTheSame(value, keyword, domain) {
    if (value.keyword.value === keyword.value && value.domain === domain) {
        return true;
    }
    return false;
}

async function checkAndSendGoogle(result, keyword, domain, position, competitor) {
    const element = result.find((value) => findTheSame(value, keyword, domain));
    if (element === undefined) {
        result.push({ keyword: keyword, domain, position })
        const res = await fetch('http://localhost:3000/api/results', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify({
            isAdd: false,
            searchEngine: 'google.com',
            keywordId: keyword.id,
            competitorId: competitor ? competitor.id : null,
            position,
            })
        })
    }
}

async function checkAndSendYandex(result, keyword, domain, position, competitor) {
    const element = result.find((value) => findTheSame(value, keyword, domain));
    if (element === undefined) {
        result.push({ keyword: keyword, domain, position })
        const res = await fetch('http://localhost:3000/api/results', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            isAdd: false,
            searchEngine: 'yandex.ru',
            keywordId: keyword.id,
            competitorId: competitor ? competitor.id : null,
            position,
            })
        })
    }
}

async function getAllProjects() {
    try {
        const res = await fetch('http://localhost:3000/api/projects/bot')
        const projects = await res.json();
        for (const project of projects) {
            await runJob(project)
        }
    } catch (e) {
    }
}

cron.schedule('0 10 * * *', () => {
    getAllProjects();
}, {
    timezone: 'Europe/Moscow',
});

async function runJob(project){
    try {
        const { competitors, keywords, domain, id } = project
        const browser = await puppeteer.launch( { headless: true, slowMo: 10 } );
        const page = await browser.newPage();
        // page.setJavaScriptEnabled(false)
        const blocker = await PuppeteerBlocker.fromPrebuiltAdsAndTracking(fetch)
        blocker.enableBlockingInPage(page);

        for (let i = 0; i < keywords.length; i++) {
            await page.goto(`https://www.google.com/search?q=${keywords[i].value}`);
            const result = [];

            const elements = await page.$$('div.g')

            for (let j = 0; j < elements.length; j++) {
                const element = elements[j];
                const [ cite ] = await element.$$("cite");
                if (cite) {
                    const text = await page.evaluate(el => el.textContent, cite);
                    if (domain.includes(text) || text.includes(domain)) {
                        await checkAndSendGoogle(result, keywords[i], domain, j+1)
                    }
                    for (const competitor of competitors) {
                        if (competitor.domain.includes(text) || text.includes(competitor.domain)) {
                            await checkAndSendGoogle(result, keywords[i], competitor.domain, j+1, competitor)
                        }
                    }
                }
            }
            const element = result.find((value) => findTheSame(value, keywords[i], domain));

            if (element === undefined) {
                await checkAndSendGoogle(result, keywords[i], domain, null)
            }

            for (const competitor of competitors) {
                await checkAndSendGoogle(result, keywords[i], competitor.domain, null, competitor)
            }
        }

        for (let i = 0; i < keywords.length; i++) {
            const result = [];

            await page.goto(`https://yandex.ru/search/?text=${keywords[i].value}`);

            const elements = await page.$$('li.serp-item')

            for (let j = 0; j < elements.length; j++) {
                const element = elements[j];
                const [ cite ] = await element.$$("a.Link_theme_outer > b");
                if (cite) {
                    const text = await page.evaluate(el => el.textContent, cite);
                    if (domain.includes(text) || text.includes(domain)) {
                        await checkAndSendYandex(result, keywords[i], domain, j+1)
                    }

                    for (const competitor of competitors) {
                        if (competitor.domain.includes(text) || text.includes(competitor.domain)) {
                            await checkAndSendYandex(result, keywords[i], competitor.domain, j+1, competitor)
                        }
                    }
                }
            }

            const element = result.find((value) => findTheSame(value, keywords[i], domain));

            if (element === undefined) {
                await checkAndSendYandex(result, keywords[i], domain, null)
            }

            for (const competitor of competitors) {
                await checkAndSendYandex(result, keywords[i], competitor.domain, null, competitor)
            }
        }
        await browser.close();
    } catch (e) {
        console.log(e)
    }

}

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const port = 3001

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/', async (req, res) => {
    await runJob(req.body)
    res.send(req.body)
})

app.listen(port)

export const roles = [1, 2] as const;

export type Role = typeof roles[number];

export type User = {
  id: number;
  email: string;
  role: Role;
}

export type SharedUser = {
  id: number;
  user: User;
}

export const parseRole = (role: Role): 'Админ' | 'Пользователь' => {
  if (role === 2) {
    return 'Админ';
  }

  return 'Пользователь';
};

export const parseToNumbers = (role: 'Админ' | 'Пользователь'): Role => {
  if (role === 'Админ') {
    return 2;
  }

  return 1;
};

import { ID } from './api';
import { KeywordGroup } from './keywordGroup';
import { ResultExtended } from './results';


export type Keyword = ID & {
  value: string;
}

export type KeywordRelation = {
  id: number;
  group: KeywordGroup;
}

export type KeywordRelationExtended = KeywordRelation & {
  keyword: Keyword;
}

export type KeywordWithRelations = Keyword & {
  relations: KeywordRelation[];
}

export type KeywordExtended = Keyword & {
  results: ResultExtended[];
}

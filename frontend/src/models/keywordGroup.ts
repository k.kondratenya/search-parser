import { Project } from './projects';

export type KeywordGroup = {
  id: number;
  name: string;
  color: string;
  project: Project;
}

export type KeywordGroupWithCount = {
  id: number;
  name: string;
  color: string;
  keywordCount: number;
}

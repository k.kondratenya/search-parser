import { ID } from './api';
import { Competitor } from './competitors';
import { KeywordGroup } from './keywordGroup';
import { KeywordWithRelations } from './keywords';

export type Project = ID & {
  name: string;
  domain: string;
  periodicity: 1 | 7 | 30;
}

export type ProjectExtended = Project & {
  competitors: Competitor[];
  keywords: KeywordWithRelations[];
}

export type ProjectExtendedWithGroups = Project & {
  competitors: Competitor[];
  keywords: KeywordWithRelations[];
  groups: KeywordGroup[];
}

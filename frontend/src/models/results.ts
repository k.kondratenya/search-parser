import { ID } from './api';
import { Competitor } from './competitors';

export type Result = ID & {
  position: number | null,
  date: string,
  searchEngine: 'google.com' | 'yandex.ru',
}

export type ResultExtended = Result & {
  competitor: Competitor | null;
}

export const tops = [1, 3, 5, 10, 20, 100] as const;
export const searchEngines = ['google.com', 'yandex.ru'] as const;

export type SearchEngine = typeof searchEngines[number];

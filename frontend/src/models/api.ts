export type ID<T = number> = {
  id: T;
}

export type PostArgs<T, G = number> = ID<G> & {
  args: T;
}

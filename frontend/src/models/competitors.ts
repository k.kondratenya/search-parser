import { ID } from './api';


export type Competitor = ID & {
  domain: string;
}

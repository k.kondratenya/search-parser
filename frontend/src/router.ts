import { createRouter, createWebHistory, RouteRecordRaw, RouterView } from 'vue-router';
import AppMiddleware from '@/AppMiddleware.vue';
import Login from '@/views/Login.vue';
import Logs from '@/views/Logs.vue';
import Projects from '@/views/Projects.vue';
import ProjectView from '@/views/ProjectView.vue';
import ProjectViewByGroup from '@/views/ProjectViewByGroup.vue';
import ResetPassword from '@/views/ResetPassword.vue';
import SignUp from '@/views/SignUp.vue';
import Users from '@/views/Users.vue';

const appMiddlewareChildren: RouteRecordRaw[] = [
  {
    path: '',
    component: RouterView,
    children: [
      {
        path: '',
        name: 'projects',
        component: Projects,
      },
      {
        path: ':id',
        name: 'project-view',
        component: ProjectView,
      },
      {
        path: 'group/:id',
        name: 'project-view-by-group',
        component: ProjectViewByGroup,
      },
      {
        path: '/logs',
        name: 'logs',
        component: Logs,
      },
      {
        path: '/users',
        name: 'users',
        component: Users,
      },
    ],
  },
];

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'index',
    component: AppMiddleware,
    children: [...appMiddlewareChildren],
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: SignUp,
  },
  {
    path: '/forgot-password',
    name: 'forgot-password',
    component: ResetPassword,
  },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});

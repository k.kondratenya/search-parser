export const codes = {
  CodeIdNotExists: 1,
  CodeInvalidId: 2,
  CodeForbidden: 3,
  CodeUnknownError: 4,
  CodeBackendError: 5,
  CodeNotImplemented: 6,
  CodeUserNotFound: 7,

  CodeUndefined: 1000,
  CodeSomethingWentWrong: 1001,

  CodeNothingToUpload: 5001,
  CodeEmptyUploadToken: 5002,
} as const;

type CodeKey = keyof typeof codes;
export type Code = typeof codes[CodeKey];

export type APIError = {
  error: {
    code: Code;
    msg: string;
  };
}
type APIErrorFn = () => APIError;

export const translateAPIError = (data: APIError | ''): void => {
  if (typeof data === 'string') {
    return;
  }
  if (data.error.code === codes.CodeUndefined && data.error.msg !== '') {
    return;
  }

  const message = `errors.${data.error.code.toString()}`;
  if (message) {
    data.error.msg = message;
  }
};

const makeAPIError = (key: CodeKey, msg: string): APIErrorFn => {
  const data: APIError = {
    error: {
      code: codes[key],
      msg,
    },
  };
  return (): APIError => {
    translateAPIError(data);
    return data;
  };
};

export const errSomethingWentWrong: APIErrorFn = makeAPIError('CodeSomethingWentWrong', 'Something went wrong');

export const errNothingToUpload: APIErrorFn = makeAPIError('CodeNothingToUpload', 'Nothing to upload');
export const errEmptyUploadToken: APIErrorFn = makeAPIError('CodeEmptyUploadToken', 'Empty upload token');

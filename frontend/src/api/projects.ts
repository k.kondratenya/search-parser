import { ID, PostArgs } from '@/models/api';
import { Project, ProjectExtended, ProjectExtendedWithGroups } from '@/models/projects';

export type CreateProjectArgs = Omit<Project, 'id'>;

export type CreateProjectReply = Project;

export type GetProjectsReply = ProjectExtended[];

export type UpdateProjectArgs = PostArgs<Omit<Project, 'id'>>;

export type UpdateProjectReply = Project;

export type GetProjectArgs = ID;

export type GetProjectReply = ProjectExtendedWithGroups;

export type DeleteProjectArgs = ID;

import { KeywordGroup } from '@/models/keywordGroup';
import { KeywordRelationExtended } from '@/models/keywords';

export type CreateKeywordGroupArgs = Omit<KeywordGroup, 'id' | 'project'> & {
  project: number;
};

export type CreateKeywordGroupReply = KeywordGroup;

export type GetKeywordGroupsArgs = {
  project: number
}

export type GetKeywordGroupsReply = KeywordGroup[];

export type DeleteKeywordGroupArgs = {
  project: number;
  id: number;
}

export type CreateKeywordRelationArgs = {
  keyword: number;
  group: number;
}

export type CreateKeywordRelationReply = KeywordRelationExtended;

export type DeleteKeywordRelationArgs = {
  id: number;
}

export const getKeywordGroupColor = (group: KeywordGroup): string => {
  if (['#DDE6E8'].includes(group.color)) {
    return '#000';
  }

  return '#FFF';
};

import { User, SharedUser } from '@/models/user';

export type UpdateUserByHimselfArgs = Omit<User, 'id' | 'role'>;

export type UpdateUserByHimselfReply = User;

export type ChangePasswordArgs = {
  previosPassword: string;
  newPassword: string;
}

export type SearchUserByEmailArgs = {
  email: string;
}

export type SearchUserByEmailReply = User[];

export type AddSharedUserArgs = {
  projectId: number;
  userId: number;
}

export type AddSharedUserReply = SharedUser

export type GetSharedUsersArgs = {
  projectId: number;
}

export type GetSharedUsersReply = SharedUser[];

export type DeleteSharedUserArgs = {
  projectId: number;
  sharedUserId: number;
}

export type ForgotPasswordArgs = {
  email: string;
}

export type ResetPasswordArgs = {
  code: number;
  email: string;
  password: string;
}

export type UpdateUserByAdmin = User;

import { ID } from '@/models/api';
import { KeywordGroupWithCount } from '@/models/keywordGroup';
import { Keyword, KeywordExtended } from '@/models/keywords';

export type GetKeywordsArgs = ID & {
  year: number;
  month: number;
  competitorID?: number;
  groups: number[];
  keywordsIncluded: number[];
  keywordsExcluded: number[];
};

export type GetKeywordsReply = KeywordExtended[];

export type CreateKeywordArgs = Omit<Keyword, 'id'> & {
  projectId: number;
};

export type CreateKeywordReply = Keyword;

export type DeleteKeywordArgs = ID;

export type FastResultArgs = ID;

export type FastResultReply = KeywordExtended[];

export type TopResultArgs = {
  competitor: number | undefined;
  groups: number[];
  id: number;
  startYear: number;
  startMonth: number;
  endYear: number;
  endMonth: number;
  keywordsIncluded: number[];
  keywordsExcluded: number[];
}

export type TopResultReply = {
  keywords: Keyword[];
  topResults: {
    searchEngine: string,
    keywordId: number,
    monthYear: string,
    top1: number,
    top3: number,
    top5: number,
    top10: number,
    top20: number,
    top100: number
  }[]
}

export type TopResultReplyByGroup = {
  groups: KeywordGroupWithCount[];
  topResults: {
    searchEngine: string,
    groupId: number,
    monthYear: string,
    top1: number,
    top3: number,
    top5: number,
    top10: number,
    top20: number,
    top100: number
  }[]
}

import { ID } from '@/models/api';
import { User } from '@/models/user';
import { AxiosReply, AxiosRequestConfig, rest } from '@/utils/rest';
import { AddSharedUserArgs, AddSharedUserReply, ChangePasswordArgs, DeleteSharedUserArgs, GetSharedUsersArgs, GetSharedUsersReply, SearchUserByEmailArgs, SearchUserByEmailReply, UpdateUserByAdmin, UpdateUserByHimselfArgs, UpdateUserByHimselfReply } from '../user';

export const getUser = (config?: AxiosRequestConfig): AxiosReply<User> =>
  rest.get('/api/authentication/get-user', config);

export const changePassword = (args: ChangePasswordArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.put('/api/authentication/change-password', args, config);

export const getUsers = (config?: AxiosRequestConfig): AxiosReply<User[]> =>
  rest.get('/api/users', config);

export const deleteUser = (args: ID,config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.delete(`/api/users/${args.id}`, config);

export const updateUserByHimselfUser = (args: UpdateUserByHimselfArgs, config?: AxiosRequestConfig): AxiosReply<UpdateUserByHimselfReply> =>
  rest.put('/api/users', args, config);

export const updateUser = ({ id, ...args }: UpdateUserByAdmin, config?: AxiosRequestConfig): AxiosReply<UpdateUserByHimselfReply> =>
  rest.put(`/api/users/admin/${id}`, args, config);

export const searchUserByEmail = ({ email }: SearchUserByEmailArgs, config?: AxiosRequestConfig): AxiosReply<SearchUserByEmailReply> =>
  rest.get(`/api/users/search?email=${email}`, config);

export const addSharedUser = ({ userId, projectId }: AddSharedUserArgs, config?: AxiosRequestConfig): AxiosReply<AddSharedUserReply> =>
  rest.post(`/api/projects/shared/${projectId}`, { userId }, config);

export const getSharedUsersByProject = ({ projectId }: GetSharedUsersArgs, config?: AxiosRequestConfig): AxiosReply<GetSharedUsersReply> =>
  rest.get(`/api/projects/shared/${projectId}/users`, config);

export const deleteSharedUsersByProject = ({ projectId, sharedUserId }: DeleteSharedUserArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.delete(`/api/projects/shared/${projectId}/users/${sharedUserId}`, config);

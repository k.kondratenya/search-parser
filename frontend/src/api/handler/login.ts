import { User } from '@/models/user';
import { AxiosReply, AxiosRequestConfig, rest } from '@/utils/rest';
import { ForgotPasswordArgs, ResetPasswordArgs } from '../user';
export const login = (args: { email: string, password: string}, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.post('/api/authentication/log-in', args, config);

export const logout = (config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.post('/api/authentication/log-out', {}, config);

export const signup = (args: Omit<User, 'id' | 'role'>, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.post('api/authentication/register', args, config);

export const forgotPassword = (args: ForgotPasswordArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.get(`/api/authentication/forgot-password?email=${args.email}`, config);

export const resetPassword = (args: ResetPasswordArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.put('/api/authentication/reset-password', args, config);

import { AxiosReply, AxiosRequestConfig, rest } from '@/utils/rest';
import { CreateProjectArgs, CreateProjectReply, DeleteProjectArgs, GetProjectArgs, GetProjectReply, GetProjectsReply, UpdateProjectArgs, UpdateProjectReply } from '../projects';

export const getProjects = (config?: AxiosRequestConfig): AxiosReply<GetProjectsReply> =>
  rest.get('/api/projects/', config);

export const getProjectsShared = (config?: AxiosRequestConfig): AxiosReply<GetProjectsReply> =>
  rest.get('/api/projects?isShared="true"', config);

export const getProject = ({ id }: GetProjectArgs, config?: AxiosRequestConfig): AxiosReply<GetProjectReply> =>
  rest.get(`/api/projects/${id}`, config);

export const createProject = (args: CreateProjectArgs, config?: AxiosRequestConfig): AxiosReply<CreateProjectReply> =>
  rest.post('/api/projects/', args, config);

export const updateProject = ({ id, args }: UpdateProjectArgs, config?: AxiosRequestConfig): AxiosReply<UpdateProjectReply> =>
  rest.put(`/api/projects/${id}`, args, config);

export const deleteProject = ({ id }: DeleteProjectArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.delete(`/api/projects/${id}`, config);

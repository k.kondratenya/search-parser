import { AxiosReply, AxiosRequestConfig, rest } from '@/utils/rest';
import { CreateCompetitorArgs, CreateCompetitorReply, DeleteCompetitorArgs, GetCompetitorsByProjectIDArgs, GetCompetitorsByProjectIDReply } from '../competitors';

export const createCompetitor = (args: CreateCompetitorArgs, config?: AxiosRequestConfig): AxiosReply<CreateCompetitorReply> =>
  rest.post('/api/competitors', args, config);

export const getCompetitorsByProjectID = ({ projectID }: GetCompetitorsByProjectIDArgs, config?: AxiosRequestConfig): AxiosReply<GetCompetitorsByProjectIDReply> =>
  rest.get(`/api/competitors/project/${projectID}`, config);

export const deleteCompetitor = ({ id }: DeleteCompetitorArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.delete(`/api/competitors/${id}`, config);

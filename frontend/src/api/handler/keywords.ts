import { AxiosReply, AxiosRequestConfig, rest } from '@/utils/rest';
import { CreateKeywordGroupArgs, CreateKeywordGroupReply, CreateKeywordRelationArgs, CreateKeywordRelationReply, DeleteKeywordGroupArgs, DeleteKeywordRelationArgs, GetKeywordGroupsArgs, GetKeywordGroupsReply } from '../keywordGroups';
import { CreateKeywordArgs, CreateKeywordReply, DeleteKeywordArgs, GetKeywordsArgs, GetKeywordsReply, FastResultArgs, FastResultReply, TopResultArgs, TopResultReply, TopResultReplyByGroup } from '../keywords';

export const getKeywords = (args: GetKeywordsArgs, config?: AxiosRequestConfig): AxiosReply<GetKeywordsReply> =>
  rest.post('/api/keywords/get-all', args, config);

export const createKeyword = (args: CreateKeywordArgs, config?: AxiosRequestConfig): AxiosReply<CreateKeywordReply> =>
  rest.post('/api/keywords', args, config);

export const deleteKeyword = ({ id }: DeleteKeywordArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.delete(`/api/keywords/${id}`, config);

export const fastResult = ({ id }: FastResultArgs, config?: AxiosRequestConfig): AxiosReply<FastResultReply> =>
  rest.post(`/api/results/fast/${id}`, {}, config);

export const createKeywordGroup = (args: CreateKeywordGroupArgs, config?: AxiosRequestConfig): AxiosReply<CreateKeywordGroupReply> =>
  rest.post('/api/keywords/group', args, config);

export const getKeywordGroups = (args: GetKeywordGroupsArgs, config?: AxiosRequestConfig): AxiosReply<GetKeywordGroupsReply> =>
  rest.get(`/api/keywords/group/${args.project}`, config);

export const deleteKeywordGroups = (args: DeleteKeywordGroupArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.delete(`/api/keywords/group/${args.project}?id=${args.id}`, config);

export const createKeywordRelation = (args: CreateKeywordRelationArgs, config?: AxiosRequestConfig): AxiosReply<CreateKeywordRelationReply> =>
  rest.post('/api/keywords/relation', args, config);

export const deleteKeywordRelation = ({ id }: DeleteKeywordRelationArgs, config?: AxiosRequestConfig): AxiosReply<''> =>
  rest.delete(`/api/keywords/relation/${id}`, config);

export const getTopResult = (args: TopResultArgs, config?: AxiosRequestConfig): AxiosReply<TopResultReply> =>
  rest.post('/api/results/top-results', args, config);

export const getTopResultByGroup = (args: TopResultArgs, config?: AxiosRequestConfig): AxiosReply<TopResultReplyByGroup> =>
  rest.post('/api/results/top-results-by-group', args, config);

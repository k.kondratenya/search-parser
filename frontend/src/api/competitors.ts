import { ID } from '@/models/api';
import { Competitor } from '@/models/competitors';

export type CreateCompetitorArgs = Omit<Competitor, 'id'> & {
  projectId: number;
};

export type CreateCompetitorReply = Competitor;

export type DeleteCompetitorArgs = ID;

export type GetCompetitorsByProjectIDArgs = {
  projectID: number;
}

export type GetCompetitorsByProjectIDReply = Competitor[]

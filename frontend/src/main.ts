import PrimeVue from 'primevue/config';
import ConfirmationService from 'primevue/confirmationservice';
import PrimeConfirmDialog from 'primevue/confirmdialog';
import PrimeToast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
import { createApp } from 'vue';
import App from './App.vue';
import { router } from './router';
const app = createApp(App);

app.use(PrimeVue, {
  ripple: true,
  locale: {
    accept: 'Да',
    reject: 'Нет',
    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь','Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    firstDayOfWeek: 1,
    dayNamesMin: [
      'Su','Mo','Tu','We','Th','Fr','Sa',
    ],
  },
});
app.use(ConfirmationService);
app.use(ToastService);
app.use(router);
app.component('prime-confirmation-dialog', PrimeConfirmDialog);
app.component('prime-toast', PrimeToast);
app.mount('#app');

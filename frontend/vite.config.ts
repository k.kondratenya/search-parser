import * as path from 'path';
import vue from '@vitejs/plugin-vue';
import { defineConfig } from 'vite';

export default defineConfig({
  plugins: [vue()],
  build: {
    outDir: '../backend/build',
  },
  server: {
    port: 8088,
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:80',
        timeout: 300000,
      },
    },
  },
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src'),
      },
      { find: 'json2csv', replacement: 'json2csv/dist/json2csv.umd.js' },
    ],
  },
});

import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  Unique,
} from 'typeorm';
import Competitor from '../competitor/competitor.entity';
import Keyword from '../keyword/keyword.entity';

@Entity('result')
@Unique(['date', 'searchEngine', 'keyword', 'competitor'])
class Result {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'int', nullable: true })
  public position: number;

  @CreateDateColumn({ type: 'date' })
  public date: Date;

  @Column({ type: 'varchar', length: 50 })
  public searchEngine: string;

  @ManyToOne(() => Competitor, (competitor) => competitor.results, {
    onDelete: 'CASCADE',
  })
  public competitor: Competitor;

  @ManyToOne(() => Keyword, (keyword) => keyword.results, {
    onDelete: 'CASCADE',
  })
  public keyword: Keyword;
}

export default Result;

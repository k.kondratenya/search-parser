import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import Result from './result.entity';
import { Repository } from 'typeorm';
import { CreateResult, GetResultsTopArgs, GetResultsTopByGroupReply, GetResultsTopReply,CreateResultAndCheck } from './dto';
import { KeywordService } from 'src/keyword/keyword.service';
import { CompetitorService } from 'src/competitor/competitor.service';
import ProjectsService from 'src/project/project.service';
import User from 'src/users/user.entity';

@Injectable()
export class ResultService {
  constructor(
    @InjectRepository(Result)
    private resultRepository: Repository<Result>,
    @Inject(KeywordService)
    private readonly keywordService: KeywordService,
    @Inject(CompetitorService)
    private readonly competitorService: CompetitorService,
    @Inject(ProjectsService)
    private readonly projectService: ProjectsService,
  ) {}

  getAllResults(): Promise<Result[]> {
    return this.resultRepository.find({
      relations: ['keyword', 'competitor'],
    });
  }

  async createResult({
    keywordId,
    competitorId,
    position,
    ...result
  }: CreateResult): Promise<Result> {
    const keyword = await this.keywordService.getKeywordById(keywordId);
    let competitor = null;
    if (competitorId !== null) {
      competitor = await this.competitorService.getCompetitorById(competitorId);
    }

    const date = new Date().toISOString().split('T')[0];
    const isExist = await this.resultRepository.findOne({ ...result, competitor, keyword, date });
    if (isExist !== undefined) {
      console.log(isExist)
      return isExist
    }
    const newResult = this.resultRepository.create({ ...result, date, position });
    newResult.keyword = keyword;
    newResult.competitor = competitor;
    await this.resultRepository.save(newResult);
    return newResult;
  }

  async createResultAndCheckBefore({
    keywordId,
    competitorId,
    ...result
  }: CreateResultAndCheck): Promise<Result> {
    const keyword = await this.keywordService.getKeywordById(keywordId);
    let competitor = null;
    if (competitorId !== null) {
      competitor = await this.competitorService.getCompetitorById(competitorId);
    }

    const newResult = await this.resultRepository.findOne({ ...result, competitor, keyword });
    return newResult;
  }

  // async makeFastResult(projectId: number, user: User): Promise<any> {
  //   const project = await this.projectService.getProjectById(projectId);
  //   await axios.post('http://localhost:3001/', project);

  //   return [];
  // }

  async deleteResult(id: number): Promise<void> {
    await this.resultRepository.delete(id);
  }

  async test(args: GetResultsTopArgs, user: User): Promise<GetResultsTopReply> {
    await this.projectService.isUserOwner(user, args.id)
    const year = args.startYear;
    const month = args.startMonth;
    const date = new Date(`${year}-${(month.toString().length === 2 ? '' : '0') + month}`);
    const startDateTime = date.toISOString().split('T')[0];
    const endDate = new Date(`${args.endYear}-${(args.endMonth.toString().length === 2 ? '' : '0') + args.endMonth}`);
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setDate(endDate.getDate() - 1);
    const endDateTime = endDate.toISOString().split('T')[0];
    console.log(startDateTime, endDateTime)

    const tops = [1, 3, 5, 10, 20, 100];

    let queryBuilder = this.resultRepository.createQueryBuilder('results')
    .select('results.searchEngine', 'searchEngine')
    .addSelect('results_keyword.id', 'keywordId')
    .addSelect("strftime('%m-%Y', results.date)", 'monthYear')

    for (const top of tops) {
      queryBuilder = queryBuilder .addSelect(`SUM(CASE
        WHEN results.position <= ${top} THEN 1
        ELSE 0
      END)`, `top${top}`)
    }

    queryBuilder
     .andWhere("DATE(results.date) BETWEEN :startDateTime AND :endDateTime", { startDateTime, endDateTime })
    .leftJoin('results.keyword', 'results_keyword')
    .andWhere("results.position is NOT NULL")
    .leftJoin('results.competitor', 'results_competitor')
    .leftJoin('results_keyword.project', 'results_keyword_project')
    .andWhere("results_keyword_project.id = :projectID", { projectID: args.id })

    queryBuilder = args.competitor === undefined
    ? queryBuilder.andWhere("results_competitor.id is NULL")
    : queryBuilder.andWhere("results_competitor.id = :competitorID", { competitorID: args.competitor })

    if (args.groups.length > 0 && args.keywordsIncluded.length > 0) {
      queryBuilder = queryBuilder.leftJoin('results_keyword.relations', 'results_keyword_relations')
      .andWhere("(results_keyword_relations.group IN (:...groups) or results_keyword.id IN (:...keywords))", { groups: args.groups, keywords: args.keywordsIncluded })
    } else if (args.groups.length > 0) {
      queryBuilder = queryBuilder.leftJoin('results_keyword.relations', 'results_keyword_relations')
      .andWhere("results_keyword_relations.group IN (:...groups)", { groups: args.groups })
    } else if (args.keywordsIncluded.length > 0) {
      queryBuilder = queryBuilder.andWhere('results_keyword.id IN (:...keywords)', { keywords: args.keywordsIncluded })
    }

    if (args.keywordsExcluded.length > 0) {
      queryBuilder = queryBuilder.andWhere('results_keyword.id NOT IN (:...keywords2)', { keywords2: args.keywordsExcluded })
    }

    queryBuilder = queryBuilder.groupBy("results_keyword.id")
    .addGroupBy('results.searchEngine')
    .addGroupBy("strftime('%m-%Y', results.date)")
    .orderBy("strftime('%m-%Y', results.date)")

    const [ res1, res2 ] = await Promise.all([queryBuilder.getRawMany(), this.keywordService.getKeywordsByProject({ projectID: args.id, groups: args.groups, keywordsExcluded: args.keywordsExcluded, keywordsIncluded: args.keywordsIncluded }, user)])

    return {
      topResults: res1 as unknown as GetResultsTopReply['topResults'],
      keywords: res2,
    }
  }

  async test2(args: GetResultsTopArgs, user: User): Promise<GetResultsTopByGroupReply> {
    await this.projectService.isUserOwner(user, args.id)
    const year = args.startYear;
    const month = args.startMonth;
    const date = new Date(`${year}-${(month.toString().length === 2 ? '' : '0') + month}`);
    const startDateTime = date.toISOString().split('T')[0];
    const endDate = new Date(`${args.endYear}-${(args.endMonth.toString().length === 2 ? '' : '0') + args.endMonth}`);
    endDate.setMonth(endDate.getMonth() + 1);
    endDate.setDate(endDate.getDate() - 1);
    const endDateTime = endDate.toISOString().split('T')[0];

    const tops = [1, 3, 5, 10, 20, 100];

    let queryBuilder = this.resultRepository.createQueryBuilder('results')
    .select('results.searchEngine', 'searchEngine')
    .addSelect('results_keyword_relations_group.id', 'groupId')
    .addSelect("strftime('%m-%Y', results.date)", 'monthYear')

    for (const top of tops) {
      queryBuilder = queryBuilder .addSelect(`SUM(CASE
        WHEN results.position <= ${top} THEN 1
        ELSE 0
      END)`, `top${top}`)
    }

    queryBuilder = queryBuilder
    .leftJoin('results.keyword', 'results_keyword')
    .leftJoin('results.competitor', 'results_competitor')
    .leftJoin('results_keyword.project', 'results_keyword_project')
    .innerJoin('results_keyword.relations', 'results_keyword_relations')
    .leftJoin('results_keyword_relations.group', 'results_keyword_relations_group')
     .andWhere("DATE(results.date) BETWEEN :startDateTime AND :endDateTime", { startDateTime, endDateTime })
    .andWhere("results.position is NOT NULL")
    .andWhere("results_keyword_project.id = :projectID", { projectID: args.id })

    queryBuilder = args.groups.length > 0 ? queryBuilder.andWhere("results_keyword_relations_group.id IN (:...groups)", { groups: args.groups }) : queryBuilder;

    queryBuilder = queryBuilder
    .groupBy("results_keyword_relations_group.id")
    .addGroupBy('results.searchEngine')
    .addGroupBy("strftime('%m-%Y', results.date)")
    .orderBy("strftime('%m-%Y', results.date)")

    queryBuilder = args.competitor === undefined
    ? queryBuilder.andWhere("results_competitor.id is NULL")
    : queryBuilder.andWhere("results_competitor.id = :competitorID", { competitorID: args.competitor })

    const [ res1, res2 ] = await Promise.all([queryBuilder.getRawMany(), this.keywordService.getGroupWithKeywordCount({ project: args.id, groups: args.groups }, user)])

    return {
      topResults: res1 as unknown as GetResultsTopByGroupReply['topResults'],
      groups: res2,
    }
  }

  async parseReslutsFromCsv(res: unknown[], projectID: number, user: User) {
    const headers = Object.values(res[1]).map((item, index) => index === 0 ? item : item.split('.').reverse().join('-'));
    const project = await this.projectService.isUserOwner(user, projectID)
    const values = res.slice(2, res.length - 1).map(item => {
      const values = Object.values(item)
      return values.slice(1, values.length)
    });
    // console.log(headers)
    // console.log(values[25])

    let searchEngine = 'yandex.ru';
    for (const value of values) {
      if (typeof(value[0]) !== 'string' && typeof(value[1]) !== 'number') {
        searchEngine = 'google.com';
        continue;
      }

      let keyword: { id: number } = await this.keywordService.getKeywordByName(value[0]);
      if (keyword === undefined) {
        keyword = await this.keywordService.createKeyword({ projectId: project.id, value: value[0] }, user);
      }


      for (let i = 1; i < headers.length; i++) {
        const date = headers[i];
        console.log(date)
        // console.log(value[i], date, [i])

        const isExist = await this.createResultAndCheckBefore({ keywordId: keyword.id, competitorId: null, date, searchEngine: searchEngine })
        if (isExist === undefined) {
          await this.createResult({ keywordId: keyword.id, competitorId: null, date, searchEngine, position: value[i] })
        }
      }

    }
    // console.log(await this.createResultAndCheckBefore({ keywordId: 2, competitorId: null, date: '2022-06-03', searchEngine: 'google.com' }))
  }
}

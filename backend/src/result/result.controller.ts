import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import JwtAuthenticationGuard from 'src/authentication/jwt-authentication.guard';
import RequestWithUser from 'src/authentication/requestWithUser.interface';
import { CreateResult, GetResultsTopArgs } from './dto';
import { ResultService } from './result.service';
import ExcelJS from 'xlsx';
import { join } from 'path';

@Controller('results')
export class ResultController {
  constructor(private readonly resultService: ResultService) {}

  @UseGuards(JwtAuthenticationGuard)
  @Get()
  getAllResults() {
    return this.resultService.getAllResults();
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('top-results')
  test(@Body() newResult: GetResultsTopArgs, @Req() { user }: RequestWithUser) {
    return this.resultService.test(newResult, user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('top-results-by-group')
  test2(@Body() newResult: GetResultsTopArgs, @Req() { user }: RequestWithUser) {
    return this.resultService.test2(newResult, user);
  }

  // @UseGuards(JwtAuthenticationGuard)
  // @Post('/fast/:id')
  // fastCheck(@Param('id') id: string, @Req() { user }: RequestWithUser) {
  //   return this.resultService.makeFastResult(Number(id), user);
  // }

  @Post()
  createResult(@Body() newResult: CreateResult) {
    return this.resultService.createResult(newResult);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get('parse')
  async parseResults(@Query('project-id') id: string, @Req() { user }: RequestWithUser) {
    const xsl = ['may_otchot', 'big_otchot', 'april'];
    for (const file of xsl) {
      const workbook = await ExcelJS.readFile(join(__dirname, `../../src/result/${file}.xls`));
      // const res = await workbook.xlsx.readFile()
      const worksheet = workbook.Sheets[workbook.SheetNames[0]]
      const res = ExcelJS.utils.sheet_to_json(worksheet)
      await this.resultService.parseReslutsFromCsv(res, Number(id), user);
    }


    return 'yes'
  }

  @Delete(':id')
  deleteResult(@Param('id') id: string) {
    return this.resultService.deleteResult(Number(id));
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResultController } from './result.controller';
import { ResultService } from './result.service';
import ResultEntity from './result.entity';
import { KeywordModule } from 'src/keyword/keyword.module';
import { CompetitorModule } from 'src/competitor/competitor.module';
import { ProjectModule } from 'src/project/project.module';


@Module({
  imports: [
    TypeOrmModule.forFeature([ResultEntity]),
    KeywordModule,
    CompetitorModule,
    ProjectModule,
  ],
  controllers: [ResultController],
  providers: [ResultService],
  exports: [ResultService],
})
export class ResultModule {}

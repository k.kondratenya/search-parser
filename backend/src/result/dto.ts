import KeywordGroup from "src/keyword/keyword-group.entity";
import Keyword from "src/keyword/keyword.entity";

export class CreateResult {
  searchEngine: string;
  keywordId: number;
  date?: string;
  competitorId: number | null;
  position: number | null;
}

export class CreateResultAndCheck {
  searchEngine: string;
  keywordId: number;
  date: string;
  competitorId: number | null;
}

export class GetResultsTopArgs {
  competitor: number | undefined;
  groups: number[];
  id: number;
  startYear: number;
  startMonth: number;
  endYear: number;
  endMonth: number;
  keywordsIncluded: number[];
  keywordsExcluded: number[];
}

export class GetResultsTopReply {
  keywords: Keyword[];
  topResults: {
    searchEngine: string,
    keywordId: number,
    monthYear: string,
    top1: string,
    top3: string,
    top5: string,
    top10: string,
    top20: string,
    top100: string
  }
}

export class GetResultsTopByGroupReply {
  groups: KeywordGroup[];
  topResults: {
    searchEngine: string,
    groupId: number,
    monthYear: string,
    top1: string,
    top3: string,
    top5: string,
    top10: string,
    top20: string,
    top100: string
  }
}

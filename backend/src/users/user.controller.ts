import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Put,
    Query,
    Req,
    UseGuards,
  } from '@nestjs/common';
import JwtAuthenticationGuard from 'src/authentication/jwt-authentication.guard';
import RequestWithUser from 'src/authentication/requestWithUser.interface';
import RoleGuard from 'src/roles/role.guard';
import { UpdateUserDto, UpdateUserByHimselfDto} from './dto/updateUser.dto';
import User from './user.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UserController {
constructor(private readonly userService: UsersService) {}

    @UseGuards(RoleGuard('ADMIN'))
    @Get()
    getAllUsers() {
        return this.userService.getAllUsers();
    }

    @UseGuards(RoleGuard('ADMIN'))
    @Put('admin/:id')
    updateUserByAdmin(@Param('id') id: string, @Body() user: UpdateUserDto) {
        return this.userService.updateUser(Number(id), user);
    }

    @UseGuards(JwtAuthenticationGuard)
    @Put()
    updateUser(@Body() newUser: UpdateUserByHimselfDto, @Req() user: User) {
        return this.userService.updateUserByHimself(user, newUser);
    }

    @UseGuards(RoleGuard('ADMIN'))
    @Delete(':id')
    deleteResult(@Param('id') id: string) {
        return this.userService.deleteUser(Number(id));
    }

    @UseGuards(JwtAuthenticationGuard)
    @Get('search')
    searchByEmail(@Query('email') email: string, @Req() { user }: RequestWithUser) {
        return this.userService.searchUserByEmail(user, email);
    }
}

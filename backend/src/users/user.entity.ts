import SharedUser from '../project/sharedUser.entity';
import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import Project from '../project/project.entity';
import Token from '../token/token.entity';

export enum Roles {
  USER = 1,
  ADMIN = 2,
}

@Entity('user')
class User {
  @PrimaryGeneratedColumn()
  public id?: number;

  @Column({ unique: true })
  public email: string;

  @Column({ type: 'simple-enum', enum: Roles, default: Roles.USER })
  public role: Roles;

  @Column({ select: false })
  public password: string;

  @OneToMany(() => Project, (project) => project.user)
  public projects: Project[];

  @OneToMany(() => SharedUser, (shared) => shared.user)
  public sharedProjects: SharedUser[];

  @OneToMany(() => Token, (token) => token.user)
  public tokens: Token[];
}

export default User;

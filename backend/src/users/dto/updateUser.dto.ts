import { Roles } from "../user.entity";

export class UpdateUserDto {
    email: string;
    role: Roles;
}

export class UpdateUserByHimselfDto {
  email: string;
}

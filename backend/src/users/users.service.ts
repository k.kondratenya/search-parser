import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import User from './user.entity';
import CreateUserDto from './dto/createUser.dto';
import { UpdateUserDto, UpdateUserByHimselfDto} from './dto/updateUser.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>
  ) {}

  async getAllUsers() {
    return this.usersRepository.find({ order: {email: 'ASC'} });
  }

  async deleteUser(id: number) {
    await this.usersRepository.delete(id);
  }

  async updateUser(id: number, user: UpdateUserDto) {
    const updatedUser = await this.usersRepository.findOne(id);
    if (!updatedUser) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    return this.usersRepository.save({ ...updatedUser, ...user});
  }

async updateUserByHimself(user: User, newUser: UpdateUserByHimselfDto) {
    const updatedUser = await this.usersRepository.findOne(user.id);
    if (!updatedUser) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    return this.usersRepository.save({ ...updatedUser, ...newUser});
  }

  async getByEmail(email: string) {
    const user = await this.usersRepository.findOne({ email }, {
      select: ['id', 'password', 'email', 'role']
    });
    if (user) {
      return user;
    }
    throw new HttpException('User with this email does not exist', HttpStatus.NOT_FOUND);
  }

  async getById(id: number) {
    const user = await this.usersRepository.findOne({ id });
    if (user) {
      return user;
    }
    throw new HttpException('User with this id does not exist', HttpStatus.NOT_FOUND);
  }

  async create(userData: CreateUserDto) {
    const newUser = await this.usersRepository.create(userData);
    await this.usersRepository.save(newUser);
    return newUser;
  }

  async updatePassword(userID: number, newPassword: string) {
    const newUser = await this.usersRepository.update(userID, { password: newPassword });
    return newUser;
  }

  async searchUserByEmail(user: User, email: string) {
    return await this.usersRepository
      .createQueryBuilder("user")
      .where("user.email like :email", { email:`%${email}%` })
      .andWhere("user.id != :id", { id: user.id })
      .getMany();
  }
}

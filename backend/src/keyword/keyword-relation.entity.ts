import Keyword from './keyword.entity';
import { PrimaryGeneratedColumn, ManyToOne, Entity, Unique } from 'typeorm';
import KeywordGroup from './keyword-group.entity';

@Entity('keyword_relation')
@Unique(['keyword', 'group'])
export default class KeywordRelation {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => Keyword, (keyword) => keyword.relations, { onDelete: 'CASCADE' })
  public keyword: Keyword;

  @ManyToOne(() => KeywordGroup, (keywordGroup) => keywordGroup.relations, { onDelete: 'CASCADE' })
  public group: KeywordGroup;
}

import {
  Body,
  Request,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CreateKeyword, CreateKeywordGroup, CreateKeywordRelation, GetAllKeywordsArgs } from './dto';
import { KeywordService } from './keyword.service';
import JwtAuthenticationGuard from 'src/authentication/jwt-authentication.guard';
import RequestWithUser from 'src/authentication/requestWithUser.interface';

@Controller('keywords')
export class KeywordController {
  constructor(private readonly keywordService: KeywordService) {}

  @UseGuards(JwtAuthenticationGuard)
  @Post('get-all')
  getAllKeywords(
    @Request() { user }: RequestWithUser, @Body() args: GetAllKeywordsArgs) {
    return this.keywordService.getAllKeywords(args, user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post()
  createKeyword(@Body() newKeyword: CreateKeyword, @Request() { user }: RequestWithUser) {
    return this.keywordService.createKeyword(newKeyword, user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get('group/:id')
  getKeywordGroup(@Param('id') id: string, @Request() { user }: RequestWithUser) {
    return this.keywordService.getGroup({ project: Number(id), groups: [] }, user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Delete('group/:id')
  deleteKeywordGroup(@Param('id') project: string, @Request() { user }: RequestWithUser, @Query('id') id: string,) {
    return this.keywordService.deleteGroup({ project: Number(project), id: Number(id) }, user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('group')
  createKeywordGroup(@Body() newKeyword: CreateKeywordGroup, @Request() { user }: RequestWithUser) {
    return this.keywordService.createGroup(newKeyword, user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Delete('relation/:id')
  deleteKeywordRelation(@Param('id') id: string, @Request() { user }: RequestWithUser) {
    return this.keywordService.deleteKeywordRelation({ id: Number(id) });
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('relation')
  createKeywordRelation(@Body() newKeyword: CreateKeywordRelation, @Request() { user }: RequestWithUser) {
    return this.keywordService.createKeywordRelation(newKeyword);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Delete(':id')
  deleteKeyword(@Param('id') id: string) {
    return this.keywordService.deleteKeyword(Number(id));
  }
}

import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import Keyword from './keyword.entity';
import KeywordGroup from './keyword-group.entity'
import KeywordRelation from './keyword-relation.entity';
import { Repository } from 'typeorm';
import { CreateKeyword, CreateKeywordGroup, CreateKeywordRelation, CreateKeywordReply, DeleteKeywordGroup, DeleteKeywordRelation, GetAllKeywordsArgs, GetKeywordGroupList } from './dto';
import ProjectService from 'src/project/project.service';
import User from 'src/users/user.entity';

@Injectable()
export class KeywordService {
  constructor(
    @InjectRepository(KeywordGroup)
    private keywordGroupRepository: Repository<KeywordGroup>,
    @InjectRepository(KeywordRelation)
    private keywordRelationRepository: Repository<KeywordRelation>,
    @InjectRepository(Keyword)
    private keywordRepository: Repository<Keyword>,
    @Inject(ProjectService)
    private readonly projectRepository: ProjectService,
  ) {}

  async getAllKeywords({ id: projectID, year, month, competitorID, groups, keywordsExcluded, keywordsIncluded }: GetAllKeywordsArgs, user: User): Promise<Keyword[]> {
    await this.projectRepository.isSharedOrOwner(user, projectID);

    const date = new Date(`${year}-${(month.toString().length === 2 ? '' : '0') + month}`);

    const startDateTime = date.toISOString().split('T')[0];
    date.setMonth(date.getMonth() + 1);
    date.setDate(date.getDate() - 1);
    const endDateTime = date.toISOString().split('T')[0];

    let queryBuilder = this.keywordRepository.createQueryBuilder('keywords')
    .where("keywords.project = :projectID", { projectID })
    .leftJoinAndSelect('keywords.results', 'keyword_results')
    .andWhere("DATE(date) BETWEEN :startDateTime AND :endDateTime", { startDateTime, endDateTime })
    // .andWhere("(keyword_results.date < :endDateTime)", { endDateTime })
    .andWhere("(keyword_results.position is NOT NULL)", { endDateTime })
    .leftJoinAndSelect('keyword_results.competitor', 'keyword_results_competitor')

    queryBuilder = competitorID === undefined
    ? queryBuilder.andWhere("(keyword_results.competitor is NULL)")
    : queryBuilder.andWhere("(keyword_results_competitor.id = :competitorID)", { competitorID: competitorID })

    if (groups.length > 0 && keywordsIncluded.length > 0) {
      queryBuilder = queryBuilder.leftJoin('keywords.relations', 'keywords_relations')
      .andWhere("(keywords_relations.group IN (:...groups) or keywords.id IN (:...keywords))", { groups, keywords: keywordsIncluded })
    } else if (groups.length > 0) {
      queryBuilder = queryBuilder.leftJoin('keywords.relations', 'keywords_relations')
      .andWhere("keywords_relations.group IN (:...groups)", { groups })
    } else if (keywordsIncluded.length > 0) {
      queryBuilder = queryBuilder.andWhere('keywords.id IN (:...keywords)', { keywords: keywordsIncluded })
    }

    if (keywordsExcluded.length > 0) {
      queryBuilder = queryBuilder.andWhere('keywords.id NOT IN (:...keywords2)', { keywords2: keywordsExcluded })
    }
    return queryBuilder.getMany()
  }

  async getKeywordById(id: number): Promise<Keyword> {
    const keyword = await this.keywordRepository.findOne(id, {
      relations: ['project']
    });
    if (keyword) {
      return keyword;
    }

    throw new HttpException('Keyword not found', HttpStatus.NOT_FOUND);
  }

  async getKeywordByName(value: string): Promise<Keyword> {
    const keyword = await this.keywordRepository.findOne({
      value,
    })
    if (keyword) {
      return keyword;
    }
  }

  async createKeyword({
    projectId,
    ...keyword
  }: CreateKeyword, user: User): Promise<CreateKeywordReply> {
    const project = await this.projectRepository.isUserOwner(user, projectId);

    const newKeyword = this.keywordRepository.create(keyword);
    newKeyword.project = project;
    await this.keywordRepository.save(newKeyword);
    return { id: newKeyword.id, value: newKeyword.value };
  }

  async deleteKeyword(id: number): Promise<void> {
    await this.keywordRepository.delete(id);
  }

  async createGroup(args: CreateKeywordGroup, user: User): Promise<KeywordGroup> {
    const project = await this.projectRepository.isUserOwner(user, args.project)
    const keywordGroup = this.keywordGroupRepository.create({
      ...args,
      project,
    });

    keywordGroup.project = project;

    await this.keywordGroupRepository.save(keywordGroup);
    return keywordGroup;
  }

  async getGroup(args: GetKeywordGroupList, user: User): Promise<KeywordGroup[]> {
    const project = await this.projectRepository.isUserOwner(user, args.project)

    return this.keywordGroupRepository.find({
      where: {
        project,
      }
    })
  }

  async getGroupWithKeywordCount(args: GetKeywordGroupList, user: User): Promise<KeywordGroup[]> {
    await this.projectRepository.isUserOwner(user, args.project)
    let queryBuilder = this.keywordGroupRepository.createQueryBuilder('group')
    .select('group.id', 'id')
    .addSelect('group.name', 'name')
    .addSelect('group.color', 'color')
    .addSelect('Count(group_relations.id)', 'keywordCount')
    .leftJoin('group.project', 'group_project')
    .where('group_project.id = :projectId', { projectId: args.project })

    queryBuilder = args.groups.length > 0 ? queryBuilder.andWhere("group.id IN (:...groups)", { groups: args.groups }) : queryBuilder;

    queryBuilder = queryBuilder.leftJoin('group.relations', 'group_relations')
    .groupBy('group.id')
    return queryBuilder.getRawMany();
  }

  async deleteGroup(args: DeleteKeywordGroup, user: User) {
    await this.projectRepository.isUserOwner(user, args.project)
    return this.keywordGroupRepository.delete(args.id)
  }

  async createKeywordRelation(args: CreateKeywordRelation) {
    const keyword = await this.keywordRepository.findOneOrFail(args.keyword);
    const group = await this.keywordGroupRepository.findOneOrFail(args.group);

    const relation = this.keywordRelationRepository.create({
      keyword,
      group,
    })

    const newRelation = await this.keywordRelationRepository.save(relation);
    return newRelation;
  }

  async deleteKeywordRelation(args: DeleteKeywordRelation) {
    return this.keywordRelationRepository.delete(args.id);
  }

  async getKeywordsByProject({ projectID, groups, keywordsExcluded, keywordsIncluded}: { projectID: number, groups?: number[], keywordsIncluded?: number[], keywordsExcluded?: number[] }, user: User) {
    await this.projectRepository.isUserOwner(user, projectID)
    let queryBuilder = this.keywordRepository.createQueryBuilder('keywords')
    .leftJoin('keywords.project', 'keyword_project')
    .where("keyword_project.id = :projectID", { projectID: projectID })

    if (groups !== undefined && groups.length > 0 && keywordsIncluded !== undefined && keywordsIncluded.length > 0) {
      queryBuilder = queryBuilder.leftJoin('keywords.relations', 'keywords_relations')
      .andWhere("(keywords_relations.group IN (:...groups) or keywords.id IN (:...keywords))", { groups, keywords: keywordsIncluded })
    } else if (groups !== undefined && groups.length > 0) {
      queryBuilder = queryBuilder.leftJoin('keywords.relations', 'keywords_relations')
      .andWhere("keywords_relations.group IN (:...groups)", { groups })
    } else if (keywordsIncluded !== undefined && keywordsIncluded.length > 0) {
      queryBuilder = queryBuilder.andWhere('keywords.id IN (:...keywords)', { keywords: keywordsIncluded })
    }

    if (keywordsExcluded !== undefined && keywordsExcluded.length > 0) {
      queryBuilder = queryBuilder.andWhere('keywords.id NOT IN (:...keywords2)', { keywords2: keywordsExcluded })
    }
    return queryBuilder.getMany()
  }
}

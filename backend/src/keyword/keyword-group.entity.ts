import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import KeywordRelation from './keyword-relation.entity';
import Project from '../project/project.entity'

@Entity('keyword_group')
class KeywordGroup {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'varchar', length: 50 })
  public name: string;

  @Column({ type: 'varchar', length: 10 })
  public color: string;

  @OneToMany(() => KeywordRelation, (relation) => relation.group)
  public relations: KeywordRelation[];

  @ManyToOne(() => Project, (project) => project.groups, {
    onDelete: 'CASCADE',
  })
  public project: Project;
}

export default KeywordGroup;

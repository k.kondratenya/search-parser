import Result from '../result/result.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import Relation from './keyword-relation.entity'
import Project from '../project/project.entity';

@Entity('keyword')
class Keyword {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'varchar', length: 50 })
  public value: string;

  @ManyToOne(() => Project, (project) => project.keywords, {
    onDelete: 'CASCADE',
  })
  public project: Project;

  @OneToMany(() => Result, (result) => result.keyword, { cascade: true })
  public results: Result[];

  @OneToMany(() => Relation, (relation) => relation.keyword, { cascade: true })
  public relations: Relation[];
}

export default Keyword;

import { Module } from '@nestjs/common';
import { KeywordController } from './keyword.controller';
import { KeywordService } from './keyword.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import Keyword from './keyword.entity';
import KeywordGroup from './keyword-group.entity';
import KeywordRelation from './keyword-relation.entity';
import { ProjectModule } from 'src/project/project.module';

@Module({
  imports: [TypeOrmModule.forFeature([Keyword]), ProjectModule, TypeOrmModule.forFeature([KeywordGroup]), TypeOrmModule.forFeature([KeywordRelation])],
  controllers: [KeywordController],
  providers: [KeywordService],
  exports: [KeywordService],
})
export class KeywordModule {}

export class CreateKeyword {
  value: string;
  projectId: number;
}

export class CreateKeywordReply {
  id: number;
  value: string;
}

export class CreateKeywordGroup {
  name: string;
  color: string;
  project: number;
}

export class GetKeywordGroupList {
  project: number;
  groups: number[];
}

export class DeleteKeywordGroup {
  project: number;
  id: number;
}

export class CreateKeywordRelation {
  keyword: number;
  group: number;
}

export class DeleteKeywordRelation {
  id: number;
}

export class GetAllKeywordsArgs {
  id: number;
  year: number;
  month: number;
  competitorID?: number;
  groups: number[];
  keywordsIncluded: number[];
  keywordsExcluded: number[];
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as connectionOptions from '../config/orm.config';
@Module({
  imports: [
    TypeOrmModule.forRoot(connectionOptions)
  ],
})
export class DatabaseModule {}

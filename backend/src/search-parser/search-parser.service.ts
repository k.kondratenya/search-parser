import { Injectable, Inject } from '@nestjs/common';
import ProjectsService from 'src/project/project.service';
import { ResultService } from 'src/result/result.service'
import puppeteer, { Browser, Page } from 'puppeteer'
// import pluginStealth from 'puppeteer-extra-plugin-stealth';
// import randomUserAgent from 'random-useragent'
import Competitor from 'src/competitor/competitor.entity';
import Keyword from 'src/keyword/keyword.entity';
import { Cron } from '@nestjs/schedule';


type SearchResult = { keyword: Keyword, domain: string, position: number }

function findTheSame(value: SearchResult, keyword: Keyword, domain: string) {
    if (value.keyword.value === keyword.value && value.domain === domain) {
        return true;
    }
    return false;
}

const setTimeoutPromise = (timer: number): Promise<void> => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, timer)
    })
}

@Injectable()
export class SearchParserService {
    private isStarted = false;
    private browser: Browser | null = null;
    private page: Page | null = null;
    private keywordsWithError: { keyword: Keyword, competitors: Competitor[], domain: string }[] = [];

    constructor(
        @Inject(ProjectsService)
        private readonly projectService: ProjectsService,
        @Inject(ResultService)
        private readonly resultService: ResultService,
    ) {}

    // onModuleInit() {
    //     this.getAllProjects()
    // }

    @Cron('49 23 * * *', { timeZone: 'Europe/Moscow' })
    private search() {
        this.getAllProjects()
    }

    async checkAndSendGoogle(result: SearchResult[], keyword: Keyword, domain: string, position: number | null, competitor?: Competitor) {
        const element = result.find((value) => findTheSame(value, keyword, domain));
        if (element === undefined) {
            result.push({ keyword: keyword, domain, position })
            try {
                await this.resultService.createResult({
                    searchEngine: 'google.com',
                    keywordId: keyword.id,
                    competitorId: competitor ? competitor.id : null,
                    position,
                })
            } catch (e) {

            }
        }
    }

    async checkAndSendYandex(result: SearchResult[], keyword: Keyword, domain: string, position: number | null, competitor?: Competitor) {
        const element = result.find((value) => findTheSame(value, keyword, domain));
        if (element === undefined) {
            result.push({ keyword: keyword, domain, position })
            try {
                await this.resultService.createResult({
                    searchEngine: 'yandex.ru',
                    keywordId: keyword.id,
                    competitorId: competitor ? competitor.id : null,
                    position,
                })
            } catch (e) {}
        }
    }

    async getAllProjects() {
        try {
            this.isStarted = true;
            const projects = await this.projectService.getAllProjectsFullForBot();
            this.browser = await puppeteer.launch( { headless: false, args: ['--no-sandbox', '--disable-setuid-sandbox'], slowMo: 10 } );
            this.page = await this.browser.newPage();
            let counter = 0;
            for (const project of projects) {
                for (let i = 0; i < project.keywords.length; i++) {
                    const isExist: boolean[] = [];
                    for (const competitor of project.competitors) {
                        const date = new Date().toISOString().split('T')[0];
                        const promise1 = this.resultService.createResultAndCheckBefore({
                            keywordId: project.keywords[i].id,
                            competitorId: competitor.id,
                            date,
                            searchEngine: 'google.com',
                        });
                        const promise2 = this.resultService.createResultAndCheckBefore({
                            keywordId: project.keywords[i].id,
                            competitorId: competitor.id,
                            date,
                            searchEngine: 'yandex.ru',
                        });
                        const existing = await Promise.all([promise1, promise2]);
                        if (existing[0] === undefined || existing[1] === undefined) {
                            isExist.push(false);
                        } else {
                            isExist.push(true)
                        }
                    }
                    {
                        const date = new Date().toISOString().split('T')[0];
                        const promise1 = this.resultService.createResultAndCheckBefore({
                            keywordId: project.keywords[i].id,
                            competitorId: null,
                            date,
                            searchEngine: 'google.com',
                        });
                        const promise2 = this.resultService.createResultAndCheckBefore({
                            keywordId: project.keywords[i].id,
                            competitorId: null,
                            date,
                            searchEngine: 'yandex.ru',
                        });
                        const existing = await Promise.all([promise1, promise2]);
                        if (existing[0] === undefined || existing[1] === undefined) {
                            isExist.push(false);
                        } else {
                            isExist.push(true)
                        }
                    }
                    if (isExist.every(item => item)) {
                        continue;
                    }
                    if ((counter + 6) % 5 === 0) {
                        this.browser.close();
                        this.browser = null;
                        this.page = null;
                        await setTimeoutPromise(420000)
                        this.browser = await puppeteer.launch( { headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'], slowMo: 10 } );
                        this.page = await this.browser.newPage();
                    }
                    counter += 1;
                    await this.runJob(project.keywords[i], project.competitors, project.domain);
                }
            }
            for (let i = 0; i < this.keywordsWithError.length; i++) {
                if ((counter + 6) % 5 === 0) {
                    this.browser.close();
                    this.browser = null;
                    this.page = null;
                    await setTimeoutPromise(420000);
                    this.browser = await puppeteer.launch( { headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'], slowMo: 10 } );
                    this.page = await this.browser.newPage();
                }
                counter++;
                await this.runJob( this.keywordsWithError[i].keyword, this.keywordsWithError[i].competitors, this.keywordsWithError[i].domain);
            }
        } catch (e) {
            console.log(e)
        }
        finally {
            this.browser.close();
            this.browser = null;
            this.page = null;
            this.isStarted = false;
        }
    }

    async runJob(keyword: Keyword, competitors: Competitor[], domain: string ){
        try {
            // const ua = new randomUserAgent((data) => {
            //     const parsed = parse(data.userAgent);
            //     return parseInt(parsed.major) > 80 && (is(data.userAgent).firefox || is(data.userAgent).chrome)
            // })
            // this.page.setUserAgent(ua.data.userAgent)

            {
                await Promise.all([
                    this.page.waitForNavigation(),
                    this.page.goto(`https://www.google.com/search?q=${keyword.value}&num=100`),
                    this.page.waitForSelector('body'),
                ]);

                const result: SearchResult[] = [];
                const elements = await this.page.$$('div.g')
                for (let j = 0; j < elements.length; j++) {
                    const element = elements[j];
                    const [ cite ] = await element.$$("cite");
                    if (cite) {
                        const text = await this.page.evaluate(el => el.textContent, cite);
                        if (domain.includes(text) || text.includes(domain)) {
                            await this.checkAndSendGoogle(result, keyword, domain, j+1)
                        }
                        for (const competitor of competitors) {
                            if (competitor.domain.includes(text) || text.includes(competitor.domain)) {
                                await this.checkAndSendGoogle(result, keyword, competitor.domain, j+1, competitor)
                            }
                        }
                    }
                }
                const element = result.find((value) => findTheSame(value, keyword, domain));

                if (element === undefined) {
                    await this.checkAndSendGoogle(result, keyword, domain, null)
                }

                for (const competitor of competitors) {
                    await this.checkAndSendGoogle(result, keyword, competitor.domain, null, competitor)
                }
            }

            await setTimeoutPromise(10);

            {
                const result: SearchResult[] = [];
                await Promise.all([
                    this.page.waitForNavigation(),
                    this.page.goto(`https://yandex.ru/search/?text=${keyword.value}&numdoc=50`),
                    this.page.waitForSelector('body'),
                ]);

                const elements = await this.page.$$('li.serp-item')
                for (let j = 0; j < elements.length; j++) {
                    const element = elements[j];
                    const [ cite ] = await element.$$("a.Link_theme_outer > b");
                    if (cite) {
                        const text = await this.page.evaluate(el => el.textContent, cite);
                        if (domain.includes(text) || text.includes(domain)) {
                            await this.checkAndSendYandex(result, keyword, domain, j+1)
                        }

                        for (const competitor of competitors) {
                            if (competitor.domain.includes(text) || text.includes(competitor.domain)) {
                                await this.checkAndSendYandex(result, keyword, competitor.domain, j+1, competitor)
                            }
                        }
                    }
                }

                const element = result.find((value) => findTheSame(value, keyword, domain));

                if (element === undefined) {
                    await this.checkAndSendYandex(result, keyword, domain, null)
                }

                for (const competitor of competitors) {
                    await this.checkAndSendYandex(result, keyword, competitor.domain, null, competitor)
                }
            }

        } catch (e) {
            this.keywordsWithError.push({ keyword, competitors, domain });
            console.log(e)
        }
    }
}

import { Module } from '@nestjs/common';
import { ResultModule } from 'src/result/result.module';
import { KeywordModule } from 'src/keyword/keyword.module';
import { CompetitorModule } from 'src/competitor/competitor.module';
import { ProjectModule } from 'src/project/project.module';
import { SearchParserService } from './search-parser.service'

@Module({
  imports: [
    KeywordModule,
    CompetitorModule,
    ProjectModule,
    ResultModule,
  ],
  providers: [SearchParserService],
})
export class SeacrhParserModule {}

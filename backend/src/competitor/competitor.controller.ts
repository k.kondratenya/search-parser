import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { CreateCompetitor } from './dto';
import { CompetitorService } from './competitor.service';
import RoleGuard from 'src/roles/role.guard';
import RequestWithUser from 'src/authentication/requestWithUser.interface';
import JwtAuthenticationGuard from 'src/authentication/jwt-authentication.guard';

@Controller('competitors')
export class CompetitorController {
  constructor(private readonly competitorService: CompetitorService) {}


  @Get()
  getAllCompetitors() {
    return this.competitorService.getAllCompetitors();
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get('/project/:projectID')
  getCompetitorsById(@Param('projectID') projectID: string, @Req() { user }: RequestWithUser) {
    return this.competitorService.getCompetitorsByProjectId(Number(projectID), user)
  }

  @Post()
  createCompetitor(@Body() newCompetitor: CreateCompetitor) {
    return this.competitorService.createCompetitor(newCompetitor);
  }

  @Delete(':id')
  deleteCompetitor(@Param('id') id: string) {
    return this.competitorService.deleteCompetitor(Number(id));
  }
}

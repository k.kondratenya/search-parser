import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import Competitor from './competitor.entity';
import { Repository } from 'typeorm';
import { CreateCompetitor } from './dto';
import ProjectService from 'src/project/project.service';
import User from 'src/users/user.entity';

@Injectable()
export class CompetitorService {
  constructor(
    @InjectRepository(Competitor)
    private competitorRepository: Repository<Competitor>,
    @Inject(ProjectService)
    private readonly projectRepository: ProjectService,
  ) {}

  getAllCompetitors(): Promise<Competitor[]> {
    return this.competitorRepository.find();
  }

  async getCompetitorsByProjectId(projectID: number, user: User): Promise<Competitor[]> {
    await this.projectRepository.isUserOwner(user, projectID);

    return this.competitorRepository.find({
      where: {
        project: {
          id: projectID,
        }
      }
    })
  }

  async getCompetitorById(id: number): Promise<Competitor> {
    const competitor = await this.competitorRepository.findOne(id);
    if (competitor) {
      return competitor;
    }

    throw new HttpException('Competitor not found', HttpStatus.NOT_FOUND);
  }

  async createCompetitor({
    projectId,
    ...competitor
  }: CreateCompetitor): Promise<Competitor> {
    const project = await this.projectRepository.getProjectBaseById(projectId);

    const newCompetitor = this.competitorRepository.create(competitor);
    newCompetitor.project = project;
    const createdCompetitor = await this.competitorRepository.save(newCompetitor);
    return createdCompetitor;
  }

  async deleteCompetitor(id: number): Promise<void> {
    await this.competitorRepository.delete(id);
  }
}

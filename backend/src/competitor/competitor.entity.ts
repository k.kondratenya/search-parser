import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import Project from '../project/project.entity';
import Result from '../result/result.entity';

@Entity('competitor')
class Competitor {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'varchar', length: 50 })
  public domain: string;

  @ManyToOne(() => Project, (project) => project.competitors)
  public project: Project;

  @OneToMany(() => Result, (result) => result.competitor)
  public results: Result[];
}

export default Competitor;

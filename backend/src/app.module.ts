import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from '@hapi/joi';

const envModule = ConfigModule.forRoot({
  isGlobal: true,
  validationSchema: Joi.object({
    POSTGRES_HOST: Joi.string().required(),
    POSTGRES_PORT: Joi.number().required(),
    POSTGRES_USER: Joi.string().required(),
    POSTGRES_PASSWORD: Joi.string().required(),
    POSTGRES_DB: Joi.string().required(),
    JWT_SECRET: Joi.string().required(),
    JWT_EXPIRATION_TIME: Joi.string().required(),
    PORT: Joi.number(),
    SMTP_EMAIL: Joi.string().required(),
    SMTP_PASSWORD: Joi.string().required(),
    SMTP_HOST: Joi.string().required(),
  })
})
import { DatabaseModule } from './database/database.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { UsersModule } from './users/users.module';
import { ProjectModule } from './project/project.module';
import { KeywordModule } from './keyword/keyword.module';
import { CompetitorModule } from './competitor/competitor.module';
import { ResultModule } from './result/result.module';
import { SeacrhParserModule } from './search-parser/search-parser.module';
import { RouterModule, Routes } from 'nest-router';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ScheduleModule } from '@nestjs/schedule';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';

const routes: Routes = [
  {
    path: 'api',
    children: [
      AuthenticationModule,
      UsersModule,
      ProjectModule,
      KeywordModule,
      CompetitorModule,
      ResultModule,
    ]
  }
]
@Module({
  imports: [
    envModule,
    DatabaseModule,
    RouterModule.forRoutes(routes),
    AuthenticationModule,
    UsersModule,
    ProjectModule,
    KeywordModule,
    CompetitorModule,
    ResultModule,
    SeacrhParserModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'build'),
    }),
    ScheduleModule.forRoot(),
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        transport: {
          host: configService.get('SMTP_HOST'),
          port: 465,
          ignoreTLS: true,
          secure: true,
          auth: {
            user: configService.get('SMTP_EMAIL'),
            pass: configService.get('SMTP_PASSWORD'),
          }
        },
        defaults: {
          from: '"Восстановить пароль" bastion-search-parser@mail.ru'
        },
        template: {
          options: {
            strict: true,
          }
        },
      }),
    })
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

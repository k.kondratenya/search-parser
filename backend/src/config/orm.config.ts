import { join } from 'path'
import { ConnectionOptions } from 'typeorm'
import User from '../users/user.entity';
import SharedUser from '../project/sharedUser.entity';
import Project from '../project/project.entity';
import Competitor from '../competitor/competitor.entity';
import Result from '../result/result.entity';
import Keyword from '../keyword/keyword.entity';
import KeywordRelation from '../keyword/keyword-relation.entity';
import KeywordGroup from '../keyword/keyword-group.entity';
import Token from '../token/token.entity';
import { root } from '../paths'
const PROD_ENV = 'production'

const config = {
  host: process.env.POSTGRES_HOST,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  port: process.env.POSTGRES_PORT,
}

const connectionOptions: ConnectionOptions = {
  type: "sqlite",
  database:  join(root, 'line.sqlite'),
  entities: [
    User,
    Project,
    Competitor,
    Result,
    Keyword,
    SharedUser,
    KeywordGroup,
    KeywordRelation,
    Token,
  ],
  synchronize: false,
  dropSchema: false,
  migrationsRun: false,
  logging: ['warn', 'error'],
  logger: process.env.NODE_ENV === PROD_ENV ? 'file' : 'debug',
  migrations: [
    __dirname+'/..'+'/migrations/*{.ts,.js}'
  ],
  cli: {
    migrationsDir: join(__dirname, '..', 'migrations')
  }
}

export = connectionOptions

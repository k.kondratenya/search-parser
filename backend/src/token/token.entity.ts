import User from '../users/user.entity';
import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';

export enum TokenTypes {
  RESET_PASSWORD = 1,
}

@Entity('token')
class Token {
  @PrimaryColumn({ unique: true, type: 'int' })
  public code: number;

  @Column({ type: 'simple-enum', enum: TokenTypes })
  public type: TokenTypes;

  @Column({ type: 'datetime' })
  public expiredIn: Date;

  @ManyToOne(() => User, (user) => user.tokens, {
    onDelete: 'CASCADE',
  })
  public user: User
}

export default Token;

import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import Token, { TokenTypes } from './token.entity'
import { Repository, LessThan } from 'typeorm';
import User from 'src/users/user.entity';

@Injectable()
export class TokenService {
    constructor(
        @InjectRepository(Token)
        private tokensRepository: Repository<Token>
    ) {}

    async generateResetPasswordToken(user: User) {
        const date = new Date();
        date.setMinutes(date.getMinutes() + 20);
        const code = Math.floor(100000 + Math.random() * 900000);
        const newToken = this.tokensRepository.create({
            user,
            expiredIn: date,
            code,
            type: TokenTypes.RESET_PASSWORD,
        })

        await this.tokensRepository.save(newToken);

        return code;
    }

    async findToken(user: User, code: number) {
        const date = new Date();
        this.tokensRepository.findOneOrFail({
            where: {
                user,
                expiredIn: LessThan(date),
                code,
            }
        })
    }

    async deleteToken(code: number) {
        this.tokensRepository.delete(code);
    }
}

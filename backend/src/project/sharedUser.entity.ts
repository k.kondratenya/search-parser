import User from '../users/user.entity';
import { PrimaryGeneratedColumn, ManyToOne, Entity, Unique } from 'typeorm';
import Project from './project.entity';

@Entity('shared_user')
@Unique(['project', 'user'])
export default class SharedUser {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => Project, (project) => project.sharedUsers, { onDelete: 'CASCADE' })
  public project: Project;

  @ManyToOne(() => User, (user) => user.sharedProjects, { onDelete: 'CASCADE' })
  public user: User;
}

import { HttpException, HttpStatus, Injectable, Inject } from '@nestjs/common';
import { CreateProjectDTO } from './dto/createProject.dto';
import { UpdateProjectDTO } from './dto/updateProject.dto';
import { InjectRepository } from '@nestjs/typeorm';
import Project from './project.entity';
import { Repository } from 'typeorm';
import User from 'src/users/user.entity';
import SharedUser from './sharedUser.entity';
import { UsersService } from 'src/users/users.service';

@Injectable()
export default class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private projectRepository: Repository<Project>,
    @InjectRepository(SharedUser)
    private sharedUserRepository: Repository<SharedUser>,
    @Inject(UsersService)
    private usersService: UsersService
  ) {}

  async isUserOwner(user: User, projectId: number): Promise<Project> {
    const project = await this.getProjectByIdWithUser(projectId);
    if (user.id !== project.user.id) {
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }
    return project;
  }

  async isSharedOrOwner(user: User, projectId: number): Promise<Project> {
    const project = await this.getProjectByIdWithUserWithShared(projectId);

    if (project.user.id === user.id) {
      return project;
    }

    if (project.sharedUsers.some(sharedUser => sharedUser.user.id === user.id)) {
      return project;
    }

    throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);

  }

  getAllProjects(): Promise<Project[]> {
    return this.projectRepository.find();
  }

  getAllProjectsFull(user: User): Promise<Project[]> {
    return this.projectRepository.find({
      relations: ['keywords', 'competitors', 'sharedUsers', 'keywords.relations', 'keywords.relations.group'],
      where: {
        user,
      }
    });
  }

  async getSharedUser(user: User): Promise<Project[]> {
    const res = await this.sharedUserRepository.createQueryBuilder('sharedUsers')
    .leftJoinAndSelect('sharedUsers.project', 'sharedUser_project')
    .leftJoin('sharedUsers.user', 'sharedUser_user')
    .where('sharedUser_user.id = :id', { id: user.id })
    .getMany()


    return res.map(item => item.project);
  }

  async addSharedUser(user: User, sharedUserId: number, projectId: number) {
    const project = await this.isUserOwner(user, projectId);
    const sharedUser = await this.usersService.getById(sharedUserId);

    try {
      const newSharedUser = this.sharedUserRepository.create();
      newSharedUser.user = sharedUser;
      newSharedUser.project = project;

      await this.sharedUserRepository.save(newSharedUser);

      delete newSharedUser.project;

      return newSharedUser;
    } catch (e) {
      throw new HttpException('Пользователь уже добавлен', HttpStatus.NOT_FOUND);
    }
  }

  async getSharedUsers(user: User, projectId: number): Promise<SharedUser[]> {
    await this.isUserOwner(user, projectId);

    return this.sharedUserRepository.find({
      relations: ['user'],
      where: {
        project: {
          id: projectId
        }
      }
    })
  }

  async deleteSharedUser(user: User, projectId: number, deletedUserId: number) {
    await this.isUserOwner(user, projectId);

    return this.sharedUserRepository.delete(deletedUserId);
  }

  getAllProjectsFullForBot(): Promise<Project[]> {
    return this.projectRepository.find({
      relations: ['keywords', 'competitors'],
    });
  }

  async getProjectBaseById(id: number): Promise<Project> {
    const project = await this.projectRepository.findOne(id);
    if (project) {
      return project;
    }

    throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
  }

  async getProjectById(id: number): Promise<Project> {
    const project = await this.projectRepository.findOne(id, {
      relations: ['keywords', 'competitors'],
    });
    if (project) {
      return project;
    }

    throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
  }

  async getProjectByIdForUser(user: User, id: number): Promise<Project> {
    await this.isSharedOrOwner(user, id);
    const project = await this.projectRepository.createQueryBuilder('projects')
    .where('projects.id = :projectID', { projectID: id })
    .leftJoinAndSelect('projects.competitors', 'projects_competitors')
    .leftJoinAndSelect('projects.keywords', 'projects_keywords')
    .leftJoinAndSelect('projects.groups', 'projects_groups')
    .leftJoinAndSelect('projects_keywords.relations', 'projects_keywords_relations')
    .leftJoinAndSelect('projects_keywords_relations.group', 'projects_keywords_relations_group')
    // .select(['projects.id', 'projects.domain', 'projects_keywords.id', 'projects_keywords.value', 'projects_groups.id', 'projects_competitors.id', 'projects_keywords_relations.id', 'projects_keywords_relations.keyword'])
    .getMany();

    if (project[0]) {
      return project[0];
    }

    throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
  }

  async getProjectByIdWithUser(id: number): Promise<Project> {
    const project = await this.projectRepository.findOne(id, {
      relations: ['user'],
    });
    if (project) {
      return project;
    }

    throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
  }

  async getProjectByIdWithUserWithShared(id: number): Promise<Project> {
    const project = await this.projectRepository.findOne(id, {
      relations: ['user', 'sharedUsers', 'sharedUsers.user'],
    });
    if (project) {
      return project;
    }

    throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
  }

  async createProject(project: CreateProjectDTO, user: User) {
    const newProject = this.projectRepository.create(project);
    newProject.user = user;
    const createdProject = await this.projectRepository.save(newProject);
    createdProject.user = undefined;
    createdProject.keywords = [];
    createdProject.competitors = [];
    return createdProject;
  }

  async updateProject(id: number, post: UpdateProjectDTO) {
    const updatedProject = await this.projectRepository.findOne(id);
    if (!updatedProject) {
      throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
    }

    return this.projectRepository.save({ ...updatedProject, ...post});
  }

  async deleteProject(id: number): Promise<void> {
    await this.projectRepository.delete(id);
  }
}

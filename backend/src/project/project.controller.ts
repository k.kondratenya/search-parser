import {
  Body,
  Request,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  Query
} from '@nestjs/common';
import ProjectService from './project.service';
import { CreateProjectDTO } from './dto/createProject.dto';
import { UpdateProjectDTO } from './dto/updateProject.dto';
import JwtAuthenticationGuard from 'src/authentication/jwt-authentication.guard';
import RequestWithUser from 'src/authentication/requestWithUser.interface';
import { AddSharedUserDTO } from './dto/addSharedUser.dto';

@Controller('projects')
export default class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @UseGuards(JwtAuthenticationGuard)
  @Get()
  getAllProjects(@Request() { user }: RequestWithUser, @Query('isShared') isShared: boolean) {
    if (isShared) {
      return this.projectService.getSharedUser(user)
    }
    return this.projectService.getAllProjectsFull(user);
  }

  @Get('/bot')
  getAllProjectsForBot() {
    return this.projectService.getAllProjectsFullForBot();
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get(':id')
  getProjectByIdForUser(@Param('id') id: string, @Request() { user }: RequestWithUser,) {
    return this.projectService.getProjectByIdForUser(user, Number(id));
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get('/shared')
  getSharedProjects(@Req() { user }: RequestWithUser) {
    return this.projectService.getSharedUser(user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('shared/:id')
  addSharedUser(@Param('id') id: string, @Body() args: AddSharedUserDTO, @Req() { user }: RequestWithUser) {
    return this.projectService.addSharedUser(user, args.userId, Number(id));
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get('shared/:id/users')
  getSharedUsersByProject(@Param('id') id: string, @Req() { user }: RequestWithUser) {
    return this.projectService.getSharedUsers(user, Number(id));
  }

  @UseGuards(JwtAuthenticationGuard)
  @Delete('shared/:id/users/:sharedUserID')
  deleteSharedUserByProject(@Param('id') id: string, @Param('sharedUserID') sharedUserID: string, @Req() { user }: RequestWithUser) {
    return this.projectService.deleteSharedUser(user, Number(id), Number(sharedUserID));
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post()
  async createProject(@Body() post: CreateProjectDTO, @Req() request: RequestWithUser) {
    const { user } = request
    return this.projectService.createProject(post, user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Put(':id')
  async replaceProject(
    @Param('id') id: string,
    @Body() post: UpdateProjectDTO,
  ) {
    return this.projectService.updateProject(Number(id), post);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Delete(':id')
  async deleteProject(@Param('id') id: string) {
    this.projectService.deleteProject(Number(id));
  }
}

import { Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, Entity } from 'typeorm';
import Keyword from '../keyword/keyword.entity';
import KeywordGroup from '../keyword/keyword-group.entity';
import Competitor from '../competitor/competitor.entity';
import User from '../users/user.entity';
import SharedUser from './sharedUser.entity';

export enum Periodicity {
  ONE_DAY = 1,
  ONE_WEEK = 7,
  ONE_MONTH = 30,
}

@Entity('project')
class Project {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'varchar', length: 50 })
  public name: string;

  @Column({ type: 'varchar', length: 50 })
  public domain: string;

  @Column({ type: 'simple-enum', enum: Periodicity })
  public periodicity: Periodicity;

  @ManyToOne(() => User, (user) => user.projects, {
    onDelete: 'CASCADE',
  })
  public user: User;

  @OneToMany(() => SharedUser, (sharedUser) => sharedUser.project)
  public sharedUsers: SharedUser[];

  @OneToMany(() => Keyword, (keyword) => keyword.project)
  public keywords: Keyword[];

  @OneToMany(() => Competitor, (competitor) => competitor.project)
  public competitors: Competitor[];

  @OneToMany(() => KeywordGroup, (group) => group.project)
  public groups: KeywordGroup[];
}

export default Project;

export class CreateProjectDTO {
  name: string;
  domain: string;
  periodicity: 1 | 7 | 30;
}

export class UpdateProjectDTO {
  name: string;
  domain: string;
  periodicity: 1 | 7 | 30;
}

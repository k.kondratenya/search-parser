import { Module } from '@nestjs/common';
import Project from './project.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import ProjectService from './project.service';
import ProjectController from './project.controller';
import SharedUser from './sharedUser.entity';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [TypeOrmModule.forFeature([Project]), TypeOrmModule.forFeature([SharedUser]), UsersModule],
  controllers: [ProjectController, ProjectController],
  providers: [ProjectService],
  exports: [ProjectService],
})
export class ProjectModule {}

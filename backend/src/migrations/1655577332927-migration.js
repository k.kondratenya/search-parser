module.exports = class migration1655577332927 {
    name = 'migration1655577332927'

    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "competitor" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "domain" varchar(50) NOT NULL, "projectId" integer)`);
        await queryRunner.query(`CREATE TABLE "result" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "position" integer, "date" date NOT NULL DEFAULT (datetime('now')), "searchEngine" varchar(50) NOT NULL, "competitorId" integer, "keywordId" integer, CONSTRAINT "UQ_600883c76746fb73cb58528e9fe" UNIQUE ("date", "searchEngine", "keywordId", "competitorId"))`);
        await queryRunner.query(`CREATE TABLE "keyword_group" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(50) NOT NULL, "color" varchar(10) NOT NULL, "projectId" integer)`);
        await queryRunner.query(`CREATE TABLE "keyword_relation" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "keywordId" integer, "groupId" integer, CONSTRAINT "UQ_f38b2e950a98e720da143673e8a" UNIQUE ("keywordId", "groupId"))`);
        await queryRunner.query(`CREATE TABLE "keyword" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" varchar(50) NOT NULL, "projectId" integer)`);
        await queryRunner.query(`CREATE TABLE "project" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(50) NOT NULL, "domain" varchar(50) NOT NULL, "periodicity" varchar CHECK( periodicity IN ('1','7','30') ) NOT NULL, "userId" integer)`);
        await queryRunner.query(`CREATE TABLE "shared_user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "projectId" integer, "userId" integer, CONSTRAINT "UQ_087bffde3288b19c93262c54c97" UNIQUE ("projectId", "userId"))`);
        await queryRunner.query(`CREATE TABLE "token" ("code" integer PRIMARY KEY NOT NULL, "type" varchar CHECK( type IN ('1') ) NOT NULL, "expiredIn" datetime NOT NULL, "userId" integer)`);
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "email" varchar NOT NULL, "role" varchar CHECK( role IN ('1','2') ) NOT NULL DEFAULT (1), "password" varchar NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"))`);
        await queryRunner.query(`CREATE TABLE "temporary_competitor" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "domain" varchar(50) NOT NULL, "projectId" integer, CONSTRAINT "FK_fdddce7c65e5f71d14da2968a90" FOREIGN KEY ("projectId") REFERENCES "project" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_competitor"("id", "domain", "projectId") SELECT "id", "domain", "projectId" FROM "competitor"`);
        await queryRunner.query(`DROP TABLE "competitor"`);
        await queryRunner.query(`ALTER TABLE "temporary_competitor" RENAME TO "competitor"`);
        await queryRunner.query(`CREATE TABLE "temporary_result" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "position" integer, "date" date NOT NULL DEFAULT (datetime('now')), "searchEngine" varchar(50) NOT NULL, "competitorId" integer, "keywordId" integer, CONSTRAINT "UQ_600883c76746fb73cb58528e9fe" UNIQUE ("date", "searchEngine", "keywordId", "competitorId"), CONSTRAINT "FK_2cceb258aa4ea9fd9a62f2cd60a" FOREIGN KEY ("competitorId") REFERENCES "competitor" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_3d299065fa5dfa1955e0737f3e0" FOREIGN KEY ("keywordId") REFERENCES "keyword" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_result"("id", "position", "date", "searchEngine", "competitorId", "keywordId") SELECT "id", "position", "date", "searchEngine", "competitorId", "keywordId" FROM "result"`);
        await queryRunner.query(`DROP TABLE "result"`);
        await queryRunner.query(`ALTER TABLE "temporary_result" RENAME TO "result"`);
        await queryRunner.query(`CREATE TABLE "temporary_keyword_group" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(50) NOT NULL, "color" varchar(10) NOT NULL, "projectId" integer, CONSTRAINT "FK_655747a10273889d2d98186a504" FOREIGN KEY ("projectId") REFERENCES "project" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_keyword_group"("id", "name", "color", "projectId") SELECT "id", "name", "color", "projectId" FROM "keyword_group"`);
        await queryRunner.query(`DROP TABLE "keyword_group"`);
        await queryRunner.query(`ALTER TABLE "temporary_keyword_group" RENAME TO "keyword_group"`);
        await queryRunner.query(`CREATE TABLE "temporary_keyword_relation" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "keywordId" integer, "groupId" integer, CONSTRAINT "UQ_f38b2e950a98e720da143673e8a" UNIQUE ("keywordId", "groupId"), CONSTRAINT "FK_c4247977667f5a5911a0d6bb98c" FOREIGN KEY ("keywordId") REFERENCES "keyword" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_abdc5632830177a98f5866c6dc6" FOREIGN KEY ("groupId") REFERENCES "keyword_group" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_keyword_relation"("id", "keywordId", "groupId") SELECT "id", "keywordId", "groupId" FROM "keyword_relation"`);
        await queryRunner.query(`DROP TABLE "keyword_relation"`);
        await queryRunner.query(`ALTER TABLE "temporary_keyword_relation" RENAME TO "keyword_relation"`);
        await queryRunner.query(`CREATE TABLE "temporary_keyword" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" varchar(50) NOT NULL, "projectId" integer, CONSTRAINT "FK_0874086e43d3edcebefd6b7b27b" FOREIGN KEY ("projectId") REFERENCES "project" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_keyword"("id", "value", "projectId") SELECT "id", "value", "projectId" FROM "keyword"`);
        await queryRunner.query(`DROP TABLE "keyword"`);
        await queryRunner.query(`ALTER TABLE "temporary_keyword" RENAME TO "keyword"`);
        await queryRunner.query(`CREATE TABLE "temporary_project" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(50) NOT NULL, "domain" varchar(50) NOT NULL, "periodicity" varchar CHECK( periodicity IN ('1','7','30') ) NOT NULL, "userId" integer, CONSTRAINT "FK_7c4b0d3b77eaf26f8b4da879e63" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_project"("id", "name", "domain", "periodicity", "userId") SELECT "id", "name", "domain", "periodicity", "userId" FROM "project"`);
        await queryRunner.query(`DROP TABLE "project"`);
        await queryRunner.query(`ALTER TABLE "temporary_project" RENAME TO "project"`);
        await queryRunner.query(`CREATE TABLE "temporary_shared_user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "projectId" integer, "userId" integer, CONSTRAINT "UQ_087bffde3288b19c93262c54c97" UNIQUE ("projectId", "userId"), CONSTRAINT "FK_eee917539ad344b879f55e50bf4" FOREIGN KEY ("projectId") REFERENCES "project" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_11388d15bf76f68e534c236723e" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_shared_user"("id", "projectId", "userId") SELECT "id", "projectId", "userId" FROM "shared_user"`);
        await queryRunner.query(`DROP TABLE "shared_user"`);
        await queryRunner.query(`ALTER TABLE "temporary_shared_user" RENAME TO "shared_user"`);
        await queryRunner.query(`CREATE TABLE "temporary_token" ("code" integer PRIMARY KEY NOT NULL, "type" varchar CHECK( type IN ('1') ) NOT NULL, "expiredIn" datetime NOT NULL, "userId" integer, CONSTRAINT "FK_94f168faad896c0786646fa3d4a" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`INSERT INTO "temporary_token"("code", "type", "expiredIn", "userId") SELECT "code", "type", "expiredIn", "userId" FROM "token"`);
        await queryRunner.query(`DROP TABLE "token"`);
        await queryRunner.query(`ALTER TABLE "temporary_token" RENAME TO "token"`);
    }

    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "token" RENAME TO "temporary_token"`);
        await queryRunner.query(`CREATE TABLE "token" ("code" integer PRIMARY KEY NOT NULL, "type" varchar CHECK( type IN ('1') ) NOT NULL, "expiredIn" datetime NOT NULL, "userId" integer)`);
        await queryRunner.query(`INSERT INTO "token"("code", "type", "expiredIn", "userId") SELECT "code", "type", "expiredIn", "userId" FROM "temporary_token"`);
        await queryRunner.query(`DROP TABLE "temporary_token"`);
        await queryRunner.query(`ALTER TABLE "shared_user" RENAME TO "temporary_shared_user"`);
        await queryRunner.query(`CREATE TABLE "shared_user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "projectId" integer, "userId" integer, CONSTRAINT "UQ_087bffde3288b19c93262c54c97" UNIQUE ("projectId", "userId"))`);
        await queryRunner.query(`INSERT INTO "shared_user"("id", "projectId", "userId") SELECT "id", "projectId", "userId" FROM "temporary_shared_user"`);
        await queryRunner.query(`DROP TABLE "temporary_shared_user"`);
        await queryRunner.query(`ALTER TABLE "project" RENAME TO "temporary_project"`);
        await queryRunner.query(`CREATE TABLE "project" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(50) NOT NULL, "domain" varchar(50) NOT NULL, "periodicity" varchar CHECK( periodicity IN ('1','7','30') ) NOT NULL, "userId" integer)`);
        await queryRunner.query(`INSERT INTO "project"("id", "name", "domain", "periodicity", "userId") SELECT "id", "name", "domain", "periodicity", "userId" FROM "temporary_project"`);
        await queryRunner.query(`DROP TABLE "temporary_project"`);
        await queryRunner.query(`ALTER TABLE "keyword" RENAME TO "temporary_keyword"`);
        await queryRunner.query(`CREATE TABLE "keyword" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "value" varchar(50) NOT NULL, "projectId" integer)`);
        await queryRunner.query(`INSERT INTO "keyword"("id", "value", "projectId") SELECT "id", "value", "projectId" FROM "temporary_keyword"`);
        await queryRunner.query(`DROP TABLE "temporary_keyword"`);
        await queryRunner.query(`ALTER TABLE "keyword_relation" RENAME TO "temporary_keyword_relation"`);
        await queryRunner.query(`CREATE TABLE "keyword_relation" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "keywordId" integer, "groupId" integer, CONSTRAINT "UQ_f38b2e950a98e720da143673e8a" UNIQUE ("keywordId", "groupId"))`);
        await queryRunner.query(`INSERT INTO "keyword_relation"("id", "keywordId", "groupId") SELECT "id", "keywordId", "groupId" FROM "temporary_keyword_relation"`);
        await queryRunner.query(`DROP TABLE "temporary_keyword_relation"`);
        await queryRunner.query(`ALTER TABLE "keyword_group" RENAME TO "temporary_keyword_group"`);
        await queryRunner.query(`CREATE TABLE "keyword_group" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(50) NOT NULL, "color" varchar(10) NOT NULL, "projectId" integer)`);
        await queryRunner.query(`INSERT INTO "keyword_group"("id", "name", "color", "projectId") SELECT "id", "name", "color", "projectId" FROM "temporary_keyword_group"`);
        await queryRunner.query(`DROP TABLE "temporary_keyword_group"`);
        await queryRunner.query(`ALTER TABLE "result" RENAME TO "temporary_result"`);
        await queryRunner.query(`CREATE TABLE "result" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "position" integer, "date" date NOT NULL DEFAULT (datetime('now')), "searchEngine" varchar(50) NOT NULL, "competitorId" integer, "keywordId" integer, CONSTRAINT "UQ_600883c76746fb73cb58528e9fe" UNIQUE ("date", "searchEngine", "keywordId", "competitorId"))`);
        await queryRunner.query(`INSERT INTO "result"("id", "position", "date", "searchEngine", "competitorId", "keywordId") SELECT "id", "position", "date", "searchEngine", "competitorId", "keywordId" FROM "temporary_result"`);
        await queryRunner.query(`DROP TABLE "temporary_result"`);
        await queryRunner.query(`ALTER TABLE "competitor" RENAME TO "temporary_competitor"`);
        await queryRunner.query(`CREATE TABLE "competitor" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "domain" varchar(50) NOT NULL, "projectId" integer)`);
        await queryRunner.query(`INSERT INTO "competitor"("id", "domain", "projectId") SELECT "id", "domain", "projectId" FROM "temporary_competitor"`);
        await queryRunner.query(`DROP TABLE "temporary_competitor"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "token"`);
        await queryRunner.query(`DROP TABLE "shared_user"`);
        await queryRunner.query(`DROP TABLE "project"`);
        await queryRunner.query(`DROP TABLE "keyword"`);
        await queryRunner.query(`DROP TABLE "keyword_relation"`);
        await queryRunner.query(`DROP TABLE "keyword_group"`);
        await queryRunner.query(`DROP TABLE "result"`);
        await queryRunner.query(`DROP TABLE "competitor"`);
    }

}

const bcrypt = require('bcrypt')

module.exports = class migration1655577332928 {
    name = 'migration1655577332928'

    async up(queryRunner) {
        const password = await bcrypt.hash(process.env.DEFAULT_USER_PASSWORD, 10)
        await queryRunner
        .manager
        .createQueryBuilder()
        .insert()
        .into("user")
        .values({
            email: process.env.DEFAULT_USER_EMAIL,
            password,
            role: 2,
         })
        .execute()
    }

    async down(queryRunner) {
        await queryRunner
        .manager
        .createQueryBuilder()
        .delete()
        .from('user')
        .where("email = :email", { email: process.env.DEFAULT_USER_EMAIL })
        .execute()
    }
}

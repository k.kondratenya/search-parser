import { Body, Req, Controller, HttpCode, Post, UseGuards, Res, Get, Put, Headers, Query } from '@nestjs/common';
import { Response } from 'express';
import { AuthenticationService } from './authentication.service';
import RegisterDto from './dto/register.dto';
import RequestWithUser from './requestWithUser.interface';
import { LocalAuthenticationGuard } from './localAuthentication.guard';
import JwtAuthenticationGuard from './jwt-authentication.guard';
import UpdatePasswordPayload from './updatePassword.interface'
import ResetPassword from './resetPassword.interface';

@Controller('authentication')
export class AuthenticationController {
  constructor(
    private readonly authenticationService: AuthenticationService
  ) {}

  @Post('register')
  async register(@Body() registrationData: RegisterDto) {
    return this.authenticationService.register(registrationData);
  }

  @HttpCode(200)
  @UseGuards(LocalAuthenticationGuard)
  @Post('log-in')
  async logIn(@Req() request: RequestWithUser, @Res() response: Response) {
    const {user} = request;
    const cookie = this.authenticationService.getCookieWithJwtToken(user.id);
    response.setHeader('Set-Cookie', cookie);
    user.password = undefined;
    return response.send(user);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Post('log-out')
  async logOut(@Res() response: Response) {
    response.setHeader('Set-Cookie', this.authenticationService.getCookieForLogOut());
    return response.sendStatus(200);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get('get-user')
  authenticate(@Req() request: RequestWithUser) {
    const user = request.user;
    user.password = undefined;
    return user;
  }

  @Get('forgot-password')
  async forgotPassword(@Headers('origin') origin: string, @Query('email') email: string, @Query('code') code?: string) {
    if (code === undefined) {
      return this.authenticationService.sendMessage(email, origin);
    }
    return this.authenticationService.isCodeExist(email, code);
  }

  @UseGuards(JwtAuthenticationGuard)
  @Put('change-password')
  resetPassword(@Req() request: RequestWithUser, @Body() body: UpdatePasswordPayload) {
    return this.authenticationService.changePassword(request.user, body.previosPassword, body.newPassword)
  }

  @Put('reset-password')
  updatePassword(@Body() body: ResetPassword) {
    return this.authenticationService.resetPassword(body)
  }
}

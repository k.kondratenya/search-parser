interface ResetPassword {
    password: string;
    code: number;
    email: string;
  }

  export default ResetPassword;

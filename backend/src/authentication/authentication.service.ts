import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import RegisterDto from './dto/register.dto';
import * as bcrypt from 'bcrypt';
import PostgresErrorCode from '../database/postgresErrorCode.enum';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import TokenPayload from './tokenPayload.interface';
import User from 'src/users/user.entity';
import { TokenService } from 'src/token/token.service'
import { MailerService } from '@nestjs-modules/mailer';
import ResetPassword from './resetPassword.interface';

const resetHTML = (code: number, origin: string) => `
  <div>
    Чтобы воcстановить пароль, введите код <b>${code}</b> в приложении
  </div>
`

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly mailerService: MailerService,
    private readonly tokenService: TokenService,
  ) {}

  public async register(registrationData: RegisterDto) {
    const hashedPassword = await bcrypt.hash(registrationData.password, 10);
    try {
      const createdUser = await this.usersService.create({
        ...registrationData,
        password: hashedPassword
      });
      createdUser.password = undefined;
      return createdUser;
    } catch (error) {
      console.log(error)
      if (error?.code === PostgresErrorCode.UniqueViolation) {
        throw new HttpException('User with that email already exists', HttpStatus.BAD_REQUEST);
      }
      throw new HttpException('Something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public getCookieWithJwtToken(userId: number) {
    const payload: TokenPayload = { userId };
    const token = this.jwtService.sign(payload);
    return `Authentication=${token}; HttpOnly; Path=/; Max-Age=${this.configService.get('JWT_EXPIRATION_TIME')}`;
  }

  public getCookieForLogOut() {
    return `Authentication=; HttpOnly; Path=/; Max-Age=0`;
  }

  public async getAuthenticatedUser(email: string, plainTextPassword: string) {
    try {
      const user = await this.usersService.getByEmail(email);
      await this.verifyPassword(plainTextPassword, user.password);
      return user;
    } catch (error) {
      throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
    }
  }

  public async changePassword(user: User, previosPassword: string, newPassword: string) {
    const { password: hashedPassword } = await this.usersService.getByEmail(user.email);
    await this.verifyPassword(previosPassword, hashedPassword);
    const newHashedPassword = await bcrypt.hash(newPassword, 10);
    await this.usersService.updatePassword(user.id, newHashedPassword);
  }

  private async verifyPassword(plainTextPassword: string, hashedPassword: string) {
    const isPasswordMatching = await bcrypt.compare(
      plainTextPassword,
      hashedPassword
    );
    if (!isPasswordMatching) {
      throw new HttpException('Wrong credentials provided', HttpStatus.BAD_REQUEST);
    }
  }

  async sendMessage(email: string, origin: string) {
    const user = await this.usersService.getByEmail(email);
    const code = await this.tokenService.generateResetPasswordToken(user);
    await this.mailerService.sendMail({
      to: email,
      subject: 'Восстановление пароля',
      html: resetHTML(code, origin),
      context: {
        origin,
        code,
      },
    });
  }

  async resetPassword({ email, code, password }: ResetPassword) {
    const user = await this.usersService.getByEmail(email);
    await this.tokenService.findToken(user, code);
    const newHashedPassword = await bcrypt.hash(password, 10);
    await this.usersService.updatePassword(user.id, newHashedPassword);
    await this.tokenService.deleteToken(code);
    return true
  }

  async isCodeExist(email: string, code: string) {
    const user = await this.usersService.getByEmail(email);
    await this.tokenService.findToken(user, Number(code));
    return true;
  }
}
